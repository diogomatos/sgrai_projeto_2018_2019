
//SCENE
var scene = new THREE.Scene();


// globais

var contador = 0;
var moduloAtivado = 0; // desativado

//CAMERA
var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

//LIGHTS
ambientLight = new THREE.AmbientLight(0x333333);	// 0.2
light = new THREE.DirectionalLight(0xFFFFFF, 1.0);

var light = new THREE.DirectionalLight(0xffffff, 1.5);
light.position.set(1, 0, 1).normalize();
light.castShadow = true;
light.shadow.camera.near = 0.1;
light.shadow.camera.far = 25;
scene.add(light);

var light2 = new THREE.DirectionalLight(0xffffff);
light2.position.set(1, 1, 0).normalize();
scene.add(light2);

var light3 = new THREE.DirectionalLight(0xffffff);
light3.position.set(-1, -1, -1).normalize();
scene.add(light3);

var light4 = new THREE.DirectionalLight(0xffffff);
light4.position.set(-5, -5, -3).normalize();
scene.add(light4);
// direction is set in GUI

//RENDERER
var renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

// create an AudioListener and add it to the camera
var listener = new THREE.AudioListener();
camera.add(listener);

// create the PositionalAudio object (passing in the listener)
var sound = new THREE.PositionalAudio(listener);





//TEXTURAS
var loader = new THREE.TextureLoader();

var loadTexture = function (path) {
  return loader.load(`./img/${path}`);
}

var TEXTURES = {
  CARPETE_1: loadTexture('carpete.jpg'),
  DEI_1: loadTexture('dei.jpg'),
  BARBIE_1: loadTexture('barbie.jpg'),
  MADEIRA_1: loadTexture('madeira1.jpg'),
  MADEIRA_2: loadTexture('madeira2.jpg'),
  MADEIRA_3: loadTexture('madeira3.jpg'),
  MADEIRA_4: loadTexture('madeira4.jpg'),
  ACO_1: loadTexture('aco1.jpg'),
  ACO_2: loadTexture('aco2.jpg')
}

//MATERIAL aka Cor

//DEI
var dei = new THREE.MeshPhongMaterial({ color: 0x5E4122, map: TEXTURES.DEI_1 });
var carpete = new THREE.MeshPhongMaterial({ color: 0x5E4122, map: TEXTURES.CARPETE_1 });
var barbie = new THREE.MeshPhongMaterial({ color: 0x5E4122, map: TEXTURES.BARBIE_1 });

//MADEIRA
var materialMadeira = new THREE.MeshPhongMaterial({ color: 0x5E4122, map: TEXTURES.MADEIRA_1 });
var materialMadeira2 = new THREE.MeshPhongMaterial({ color: 0x5E4122, map: TEXTURES.MADEIRA_2 });
var materialMadeira3 = new THREE.MeshPhongMaterial({ color: 0x5E4122, map: TEXTURES.MADEIRA_3 });
var materialMadeira4 = new THREE.MeshPhongMaterial({ color: 0x5E4122, map: TEXTURES.MADEIRA_4 });

//ACO
var materialAco1 = new THREE.MeshPhongMaterial({ color: 0xE6E6E6, map: TEXTURES.ACO_1 });
var materialAco2 = new THREE.MeshPhongMaterial({ color: 0xE6E6E6, map: TEXTURES.ACO_2 });

//OUTROS
var castanho = new THREE.MeshPhongMaterial({ color: 0x936536 });
var verdeClaro = new THREE.MeshPhongMaterial({ color: 0x677E4E });
var torrado = new THREE.MeshPhongMaterial({ color: 0xF4CE5B });
var metal = new THREE.MeshPhongMaterial({ color: 0xA7C5CC, map: TEXTURES.METAL });


//Geometry
var geometriaChao = new THREE.BoxGeometry(400, 5, 200);
var geometriaLadosParede = new THREE.BoxGeometry(200, 5, 200);
var geometriaBase = new THREE.BoxGeometry(40, 2, 20);
var geometriaTras = new THREE.BoxGeometry(40, 80, 2);
var geometriaLados = new THREE.BoxGeometry(2, 80, 20);
var geometriaPorta = new THREE.BoxGeometry(20, 60, 2);
var geometriaLadosGaveta = new THREE.BoxGeometry(2, 10, 18);
var geometriaTrasBase = new THREE.BoxGeometry(37.5, 8, 0);
var geometriaFrenteBase = new THREE.BoxGeometry(35, 8, 0);
var geometriaPuxadorPorta = new THREE.SphereGeometry(1.2, 40, 40, 0, Math.PI * 2, 0, Math.PI * 2);
var geometriaPuxador = new THREE.SphereGeometry(1, 40, 40, 0, Math.PI * 2, 0, Math.PI * 2);
var geometriaBaseGaveta = new THREE.BoxGeometry(35, 2, 18);
var geometryBarra = new THREE.CylinderGeometry(1, 1, 40, 32);
var pivot = new THREE.BoxGeometry(0, 0, 0);

//Cenario
var chao = new THREE.Mesh(geometriaChao, carpete);
chao.translateY(-3.4);
scene.add(chao);

var paredeTras = new THREE.Mesh(geometriaChao, materialMadeira3);
paredeTras.rotateX(80.15);
paredeTras.translateZ(90);
paredeTras.translateY(100);
scene.add(paredeTras);

/*var paredeFrente = new THREE.Mesh(geometriaChao, materialMadeira4);
paredeFrente.rotateX(80);
paredeFrente.translateZ(105);
paredeFrente.translateY(-100);
scene.add(paredeFrente);*/

var paredeLadoEsq = new THREE.Mesh(geometriaLadosParede, materialMadeira3);
paredeLadoEsq.translateZ(-10);
paredeLadoEsq.translateY(40);
paredeLadoEsq.rotateZ(11);
paredeLadoEsq.translateY(-200);
paredeLadoEsq.translateX(-50);
scene.add(paredeLadoEsq);

var paredeLadoDir = new THREE.Mesh(geometriaLadosParede, materialMadeira3);
paredeLadoDir.translateZ(-10);
paredeLadoDir.translateY(40);
paredeLadoDir.rotateZ(11);
paredeLadoDir.translateY(200);
paredeLadoDir.translateX(-50);
scene.add(paredeLadoDir);


//ARMARIO
//Base
var base = new THREE.Mesh(geometriaBase, materialMadeira4);
scene.add(base);



//Tampa
var tampa = new THREE.Mesh(geometriaBase, materialMadeira4);
tampa.translateY(80);
scene.add(tampa);

//Parte de Tras
var parteTras = new THREE.Mesh(geometriaTras, materialMadeira4);
parteTras.translateZ(-10);
parteTras.translateY(40);
scene.add(parteTras);


//---------------------------------------------------------------------------------------------------
// GuideLine porta da Esquerda
var guideLineEsq = new THREE.Mesh(pivot, materialMadeira4);
guideLineEsq.translateX(-20);
guideLineEsq.translateZ(10);
scene.add(guideLineEsq);

// Pivot point lado esquerdo
pivotPointEsq = new THREE.Object3D();
pivotPointEsq.rotation.y -= Math.PI;
guideLineEsq.add(pivotPointEsq);

//Porta Esquerda
var portaEsq = new THREE.Mesh(geometriaPorta, materialMadeira4);
portaEsq.translateY(50);
portaEsq.translateX(-10);
scene.add(portaEsq);
var puxadorPortaEsquerda = new THREE.Mesh(geometriaPuxadorPorta, metal);
puxadorPortaEsquerda.translateY(45);
puxadorPortaEsquerda.translateZ(-1.9);
puxadorPortaEsquerda.translateX(-15);
scene.add(puxadorPortaEsquerda);

// make the pivotpoint parent of portaEsq
pivotPointEsq.add(portaEsq);
pivotPointEsq.add(puxadorPortaEsquerda);

//------------------------------------------------------------------------------------------------------

// GuideLine porta da Direita
var guideLineDir = new THREE.Mesh(pivot, materialMadeira4);
guideLineDir.translateX(20);
guideLineDir.translateZ(10);
scene.add(guideLineDir);

// Pivot point lado Direito
pivotPointDir = new THREE.Object3D();
pivotPointDir.rotation.y -= Math.PI;
guideLineDir.add(pivotPointDir);

//Porta Direita
var portaDir = new THREE.Mesh(geometriaPorta, materialMadeira4);
portaDir.translateY(50);
portaDir.translateX(10);
scene.add(portaDir);
var puxadorPortaDireita = new THREE.Mesh(geometriaPuxadorPorta, metal);
puxadorPortaDireita.translateY(45);
puxadorPortaDireita.translateZ(-1.9);
puxadorPortaDireita.translateX(15);
scene.add(puxadorPortaDireita);

// make the pivotpoint parent of portaDir
pivotPointDir.add(portaDir);
pivotPointDir.add(puxadorPortaDireita);


// Lado Direito
var ladoDireito = new THREE.Mesh(geometriaLados, materialMadeira4);
ladoDireito.translateY(40);
ladoDireito.translateX(20);
scene.add(ladoDireito);

//Lado Esquerdo
var ladoEsquerdo = new THREE.Mesh(geometriaLados, materialMadeira4);
ladoEsquerdo.translateY(40);
ladoEsquerdo.translateX(-20);
scene.add(ladoEsquerdo);





/*         Gaveta 1          */
//Base
var baseGaveta1 = new THREE.Mesh(geometriaBaseGaveta, materialMadeira4);
baseGaveta1.translateY(1);
baseGaveta1.translateZ(1);
scene.add(baseGaveta1);
//Lado Esquerdo
var ladoEsquerdoGaveta1 = new THREE.Mesh(geometriaLadosGaveta, materialMadeira4);
ladoEsquerdoGaveta1.translateX(-18);
ladoEsquerdoGaveta1.translateY(5);
ladoEsquerdoGaveta1.translateZ(1);
scene.add(ladoEsquerdoGaveta1);
//Lado Direito
var ladoDireitoGaveta1 = new THREE.Mesh(geometriaLadosGaveta, materialMadeira4);
ladoDireitoGaveta1.translateX(18);
ladoDireitoGaveta1.translateY(5);
ladoDireitoGaveta1.translateZ(1);
scene.add(ladoDireitoGaveta1);
//Tras
var trasGaveta1 = new THREE.Mesh(geometriaTrasBase, materialMadeira4);
trasGaveta1.translateZ(-8);
trasGaveta1.translateY(6);
scene.add(trasGaveta1);
//Frente
var frenteGaveta1 = new THREE.Mesh(geometriaFrenteBase, materialMadeira4);
frenteGaveta1.translateZ(9.5);
frenteGaveta1.translateY(6);
scene.add(frenteGaveta1);
//Puxador
var puxador = new THREE.Mesh(geometriaPuxador, metal);
puxador.translateY(5);
puxador.translateZ(10.8);
scene.add(puxador);



/*         Gaveta 2          */
//Base
var baseGaveta2 = new THREE.Mesh(geometriaBaseGaveta, materialMadeira4);
baseGaveta2.translateY(1);
baseGaveta2.translateZ(1);
baseGaveta2.translateY(10.5);
scene.add(baseGaveta2);
//Lado Esquerdo
var ladoEsquerdoGaveta2 = new THREE.Mesh(geometriaLadosGaveta, materialMadeira4);
ladoEsquerdoGaveta2.translateX(-18);
ladoEsquerdoGaveta2.translateY(5);
ladoEsquerdoGaveta2.translateZ(1);
ladoEsquerdoGaveta2.translateY(10.5);
scene.add(ladoEsquerdoGaveta2);
//Lado Direito
var ladoDireitoGaveta2 = new THREE.Mesh(geometriaLadosGaveta, materialMadeira4);
ladoDireitoGaveta2.translateX(18);
ladoDireitoGaveta2.translateY(5);
ladoDireitoGaveta2.translateZ(1);
ladoDireitoGaveta2.translateY(10.5);
scene.add(ladoDireitoGaveta2);
//Tras
var trasGaveta2 = new THREE.Mesh(geometriaTrasBase, materialMadeira4);
trasGaveta2.translateZ(-8);
trasGaveta2.translateY(6);
trasGaveta2.translateY(10.5);
scene.add(trasGaveta2);
//Frente
var frenteGaveta2 = new THREE.Mesh(geometriaFrenteBase, materialMadeira4);
frenteGaveta2.translateZ(9.5);
frenteGaveta2.translateY(6);
frenteGaveta2.translateY(10.5);
scene.add(frenteGaveta2);
//Puxador
var puxador2 = new THREE.Mesh(geometriaPuxador, metal);
puxador2.translateY(5);
puxador2.translateZ(10.8);
puxador2.translateY(10.5);
scene.add(puxador2);

//Barra 
var barra = new THREE.Mesh(geometryBarra, metal);
barra.translateY(60);
barra.rotateZ(54.98);
scene.add(barra);



camera.position.set(0, -3, 0);

var cameraControls = new Controls(camera);
scene.add(cameraControls);

var animate = function () {
  requestAnimationFrame(animate);

  //cube.rotation.x += 0.01;
  //cube.rotation.y += 0.01;

  renderer.render(scene, camera);
};

animate();


function Controls(camera) {
  var controls = new THREE.OrbitControls(camera);

  //controls.update() must be called after any manual changes to the camera's transform
  camera.position.set(0, 80, 135);
  controls.update();
}



document.body.addEventListener('keypress', keyPressed);
function keyPressed(e) {
  switch (e.key.toLowerCase()) {

    case 'm':  // Aumentar intensidade luz
      light.intensity += 0.1;
      light2.intensity += 0.1;
      light3.intensity += 0.1;
      light4.intensity += 0.1;
      break;

    case 'n': // Diminuir intensidade luz
      light.intensity -= 0.1;
      light2.intensity -= 0.1;
      light3.intensity -= 0.1;
      light4.intensity -= 0.1;
      break;

    case 'a': // Abrir gaveta
      if (baseGaveta1.position.z < 15) { // nao deixa a gaveta saltar fora
        //Gaveta 1
        baseGaveta1.translateZ(8);
        ladoEsquerdoGaveta1.translateZ(8);
        ladoDireitoGaveta1.translateZ(8);
        trasGaveta1.translateZ(8);
        frenteGaveta1.translateZ(8);
        puxador.translateZ(8);

      }
      break;
    case 'f': // Fechar gaveta
      if (baseGaveta1.position.z > 5) { // nao deixa a gaveta saltar fora
        //Gaveta 1
        baseGaveta1.translateZ(-8);
        ladoEsquerdoGaveta1.translateZ(-8);
        ladoDireitoGaveta1.translateZ(-8);
        trasGaveta1.translateZ(-8);
        frenteGaveta1.translateZ(-8);
        puxador.translateZ(-8);

      }
      break;

    case 'q': // Abrir gaveta 2 armario 1
      if (baseGaveta2.position.z < 15) { // nao deixa a gaveta saltar fora
        //Gaveta 2
        baseGaveta2.translateZ(8);
        ladoEsquerdoGaveta2.translateZ(8);
        ladoDireitoGaveta2.translateZ(8);
        trasGaveta2.translateZ(8);
        frenteGaveta2.translateZ(8);
        puxador2.translateZ(8);


      }
      break;
    case 'w': // Fechar gaveta 2  armario 1
      if (baseGaveta2.position.z > 5) { // nao deixa a gaveta saltar fora
        //Gaveta 2
        baseGaveta2.translateZ(-8);
        ladoEsquerdoGaveta2.translateZ(-8);
        ladoDireitoGaveta2.translateZ(-8);
        trasGaveta2.translateZ(-8);
        frenteGaveta2.translateZ(-8);
        puxador2.translateZ(-8);
      }
      break;

    case 'r': // Abrir gaveta modulo
      //  if (baseGaveta2Modulo.position.z < 15) { // nao deixa a gaveta saltar fora
      //Gaveta 2
      baseGaveta2Modulo.translateZ(8);
      ladoEsquerdoGaveta2Modulo.translateZ(8);
      ladoDireitoGaveta2Modulo.translateZ(8);
      trasGaveta2Modulo.translateZ(8);
      frenteGaveta2Modulo.translateZ(8);
      puxador2Modulo.translateZ(8);

      // }
      break;
    case 'f': // Fechar gaveta modulo
      if (baseGaveta2.position.z > 5) { // nao deixa a gaveta saltar fora
        //Gaveta 2
        baseGaveta2Modulo.translateZ(-8);
        ladoEsquerdoGaveta2Modulo.translateZ(-8);
        ladoDireitoGaveta2Modulo.translateZ(-8);
        trasGaveta2Modulo.translateZ(-8);
        frenteGaveta2Modulo.translateZ(-8);
        puxador2Modulo.translateZ(-8);
      }
      break;

    case 'z': // Abrir Porta
      if (contador < Math.PI && moduloAtivado == 0) {
        // load a sound and set it as the PositionalAudio object's buffer
        var audioLoader = new THREE.AudioLoader();
        audioLoader.load('songs/barbie.mp3', function (buffer) {
          sound.setBuffer(buffer);
          sound.setRefDistance(20);
        //  sound.play();
        });

        pivotPointEsq.rotation.y -= 0.05;
        puxadorPortaEsquerda.rotation.y -= 0.05;
        pivotPointDir.rotation.y += 0.05;
        puxadorPortaDireita.rotation.y += 0.05;
        contador += 0.05;
      } else if (contador < Math.PI && moduloAtivado == 1) {

        pivotPointEsqModulo.rotation.y -= 0.05;
        //   puxadorPortaEsquerdaModulo.rotation.y -= 0.05;

        pivotPointEsq.rotation.y -= 0.05;
        puxadorPortaEsquerda.rotation.y -= 0.05;
        pivotPointDir.rotation.y += 0.05;
        puxadorPortaDireita.rotation.y += 0.05;

        pivotPointDirModulo.rotation.y += 0.05;
        puxadorPortaDireitaModulo.rotation.y += 0.05;



        contador += 0.05;


      }
      break;

    case 'x': // Fechar Porta
      if (contador > 0 && moduloAtivado == 0) {
        pivotPointEsq.rotation.y += 0.05;
        puxadorPortaEsquerda.rotation.y += 0.05;
        pivotPointDir.rotation.y -= 0.05;
        puxadorPortaDireita.rotation.y -= 0.05;

        contador -= 0.05;
      } else {

        //puxadorPortaDireitaModulo.rotation.y -= 0.05;
        pivotPointDirModulo.rotation.y -= 0.05;

        pivotPointEsq.rotation.y += 0.05;
        puxadorPortaEsquerda.rotation.y += 0.05;
        pivotPointDir.rotation.y -= 0.05;
        puxadorPortaDireita.rotation.y -= 0.05;

        pivotPointEsqModulo.rotation.y += 0.05;
        puxadorPortaEsquerdaModulo.rotation.y += 0.05;



        contador -= 0.05;
      }
      break;

    case 'l':
      if (moduloAtivado == 0) {
        adicionarModulo();
      }
      break;

    case '1':
      base.material = materialMadeira;
      tampa.material = materialMadeira;
      parteTras.material = materialMadeira;
      portaEsq.material = materialMadeira;
      portaDir.material = materialMadeira;
      ladoDireito.material = materialMadeira;
      ladoEsquerdo.material = materialMadeira;

      baseGaveta1.material = materialMadeira;
      ladoEsquerdoGaveta1.material = materialMadeira;
      ladoDireitoGaveta1.material = materialMadeira;
      trasGaveta1.material = materialMadeira;
      frenteGaveta1.material = materialMadeira;

      baseGaveta2.material = materialMadeira;
      ladoEsquerdoGaveta2.material = materialMadeira;
      ladoDireitoGaveta2.material = materialMadeira;
      trasGaveta2.material = materialMadeira;
      frenteGaveta2.material = materialMadeira;
      break;

    case '2':
      base.material = materialMadeira2;
      tampa.material = materialMadeira2;
      parteTras.material = materialMadeira2;
      portaEsq.material = materialMadeira2;
      portaDir.material = materialMadeira2;
      ladoDireito.material = materialMadeira2;
      ladoEsquerdo.material = materialMadeira2;

      baseGaveta1.material = materialMadeira2;
      ladoEsquerdoGaveta1.material = materialMadeira2;
      ladoDireitoGaveta1.material = materialMadeira2;
      trasGaveta1.material = materialMadeira2;
      frenteGaveta1.material = materialMadeira2;

      baseGaveta2.material = materialMadeira2;
      ladoEsquerdoGaveta2.material = materialMadeira2;
      ladoDireitoGaveta2.material = materialMadeira2;
      trasGaveta2.material = materialMadeira2;
      frenteGaveta2.material = materialMadeira2;
      break;

    case '3':
      base.material = materialMadeira3;
      tampa.material = materialMadeira3;
      parteTras.material = materialMadeira3;
      portaEsq.material = materialMadeira3;
      portaDir.material = materialMadeira3;
      ladoDireito.material = materialMadeira3;
      ladoEsquerdo.material = materialMadeira3;

      baseGaveta1.material = materialMadeira3;
      ladoEsquerdoGaveta1.material = materialMadeira3;
      ladoDireitoGaveta1.material = materialMadeira3;
      trasGaveta1.material = materialMadeira3;
      frenteGaveta1.material = materialMadeira3;

      baseGaveta2.material = materialMadeira3;
      ladoEsquerdoGaveta2.material = materialMadeira3;
      ladoDireitoGaveta2.material = materialMadeira3;
      trasGaveta2.material = materialMadeira3;
      frenteGaveta2.material = materialMadeira3;
      break;

    case '4':
      base.material = materialMadeira4;
      tampa.material = materialMadeira4;
      parteTras.material = materialMadeira4;
      portaEsq.material = materialMadeira4;
      portaDir.material = materialMadeira4;
      ladoDireito.material = materialMadeira4;
      ladoEsquerdo.material = materialMadeira4;

      baseGaveta1.material = materialMadeira4;
      ladoEsquerdoGaveta1.material = materialMadeira4;
      ladoDireitoGaveta1.material = materialMadeira4;
      trasGaveta1.material = materialMadeira4;
      frenteGaveta1.material = materialMadeira4;

      baseGaveta2.material = materialMadeira4;
      ladoEsquerdoGaveta2.material = materialMadeira4;
      ladoDireitoGaveta2.material = materialMadeira4;
      trasGaveta2.material = materialMadeira4;
      frenteGaveta2.material = materialMadeira4;
      break;

    case '5':
      base.material = materialAco1;
      tampa.material = materialAco1;
      parteTras.material = materialAco1;
      portaEsq.material = materialAco1;
      portaDir.material = materialAco1;
      ladoDireito.material = materialAco1;
      ladoEsquerdo.material = materialAco1;

      baseGaveta1.material = materialAco1;
      ladoEsquerdoGaveta1.material = materialAco1;
      ladoDireitoGaveta1.material = materialAco1;
      trasGaveta1.material = materialAco1;
      frenteGaveta1.material = materialAco1;

      baseGaveta2.material = materialAco1;
      ladoEsquerdoGaveta2.material = materialAco1;
      ladoDireitoGaveta2.material = materialAco1;
      trasGaveta2.material = materialAco1;
      frenteGaveta2.material = materialAco1;
      break;

    case '6':
      base.material = materialAco2;
      tampa.material = materialAco2;
      parteTras.material = materialAco2;
      portaEsq.material = materialAco2;
      portaDir.material = materialAco2;
      ladoDireito.material = materialAco2;
      ladoEsquerdo.material = materialAco2;

      baseGaveta1.material = materialAco2;
      ladoEsquerdoGaveta1.material = materialAco2;
      ladoDireitoGaveta1.material = materialAco2;
      trasGaveta1.material = materialAco2;
      frenteGaveta1.material = materialAco2;

      baseGaveta2.material = materialAco2;
      ladoEsquerdoGaveta2.material = materialAco2;
      ladoDireitoGaveta2.material = materialAco2;
      trasGaveta2.material = materialAco2;
      frenteGaveta2.material = materialAco2;
      break;
  }
  e.preventDefault();
}

function adicionarModulo() {

  moduloAtivado = 1;
  //ARMARIO
  //Base
  var baseModulo = new THREE.Mesh(geometriaBase, materialMadeira4);
  baseModulo.translateX(40);
  scene.add(baseModulo);

  //Tampa
  var tampaModulo = new THREE.Mesh(geometriaBase, materialMadeira4);
  tampaModulo.translateY(80);
  tampaModulo.translateX(40);
  scene.add(tampaModulo);

  //Parte de Tras
  var parteTrasModulo = new THREE.Mesh(geometriaTras, materialMadeira4);
  parteTrasModulo.translateZ(-10);
  parteTrasModulo.translateY(40);
  parteTrasModulo.translateX(40);
  scene.add(parteTrasModulo);


  //---------------------------------------------------------------------------------------------------
  // GuideLine porta da Esquerda
  var guideLineEsqModulo = new THREE.Mesh(pivot, materialMadeira4);
  guideLineEsqModulo.translateX(20);
  guideLineEsqModulo.translateZ(10);
  scene.add(guideLineEsqModulo);

  // Pivot point lado esquerdo
  pivotPointEsqModulo = new THREE.Object3D();
  pivotPointEsqModulo.rotation.y -= Math.PI;
  guideLineEsqModulo.add(pivotPointEsqModulo);

  //Porta Esquerda
  var portaEsqModulo = new THREE.Mesh(geometriaPorta, materialMadeira4);
  portaEsqModulo.translateY(50);
  portaEsqModulo.translateX(-10);
  scene.add(portaEsqModulo);
  var puxadorPortaEsquerdaModulo = new THREE.Mesh(geometriaPuxadorPorta, metal);
  puxadorPortaEsquerdaModulo.translateY(45);
  puxadorPortaEsquerdaModulo.translateZ(-2);
  puxadorPortaEsquerdaModulo.translateX(-15);
  scene.add(puxadorPortaEsquerdaModulo);

  // make the pivotpoint parent of portaEsq
  pivotPointEsqModulo.add(portaEsqModulo);
  pivotPointEsqModulo.add(puxadorPortaEsquerdaModulo);

  //------------------------------------------------------------------------------------------------------

  // GuideLine porta da Direita
  var guideLineDirModulo = new THREE.Mesh(pivot, materialMadeira4);
  guideLineDirModulo.translateX(20);
  guideLineDirModulo.translateZ(10);
  guideLineDirModulo.translateX(40);
  scene.add(guideLineDirModulo);

  // Pivot point lado Direito
  pivotPointDirModulo = new THREE.Object3D();
  pivotPointDirModulo.rotation.y -= Math.PI;
  guideLineDirModulo.add(pivotPointDirModulo);

  //Porta Direita
  var portaDirModulo = new THREE.Mesh(geometriaPorta, materialMadeira4);
  portaDirModulo.translateY(50);
  portaDirModulo.translateX(10);
  scene.add(portaDirModulo);
  var puxadorPortaDireitaModulo = new THREE.Mesh(geometriaPuxadorPorta, metal);
  puxadorPortaDireitaModulo.translateY(45);
  puxadorPortaDireitaModulo.translateZ(-2);
  puxadorPortaDireitaModulo.translateX(15);
  scene.add(puxadorPortaDireitaModulo);

  // make the pivotpoint parent of portaDir
  pivotPointDirModulo.add(portaDirModulo);
  pivotPointDirModulo.add(puxadorPortaDireitaModulo);


  // Lado Direito
  var ladoDireito = new THREE.Mesh(geometriaLados, materialMadeira4);
  ladoDireito.translateY(40);
  ladoDireito.translateX(20);
  ladoDireito.translateX(40);
  scene.add(ladoDireito);

  //Lado Esquerdo
  var ladoEsquerdoModulo = new THREE.Mesh(geometriaLados, materialMadeira4);
  ladoEsquerdoModulo.translateY(40);
  ladoEsquerdoModulo.translateX(-20);
  ladoEsquerdoModulo.translateX(40);
  scene.add(ladoEsquerdoModulo);





  /*         Gaveta 1          */
  //Base
  var baseGaveta1Modulo = new THREE.Mesh(geometriaBaseGaveta, materialMadeira4);
  baseGaveta1Modulo.translateY(1);
  baseGaveta1Modulo.translateZ(1);
  baseGaveta1Modulo.translateX(40);
  scene.add(baseGaveta1Modulo);
  //Lado Esquerdo
  var ladoEsquerdoGaveta1Modulo = new THREE.Mesh(geometriaLadosGaveta, materialMadeira4);
  ladoEsquerdoGaveta1Modulo.translateX(-18);
  ladoEsquerdoGaveta1Modulo.translateY(5);
  ladoEsquerdoGaveta1Modulo.translateZ(1);
  ladoEsquerdoGaveta1Modulo.translateX(40);
  scene.add(ladoEsquerdoGaveta1Modulo);
  //Lado Direito
  var ladoDireitoGaveta1Modulo = new THREE.Mesh(geometriaLadosGaveta, materialMadeira4);
  ladoDireitoGaveta1Modulo.translateX(18);
  ladoDireitoGaveta1Modulo.translateY(5);
  ladoDireitoGaveta1Modulo.translateZ(1);
  ladoDireitoGaveta1Modulo.translateX(40);
  scene.add(ladoDireitoGaveta1Modulo);
  //Tras
  var trasGaveta1Modulo = new THREE.Mesh(geometriaTrasBase, materialMadeira4);
  trasGaveta1Modulo.translateZ(-8);
  trasGaveta1Modulo.translateY(6);
  trasGaveta1Modulo.translateX(40);
  scene.add(trasGaveta1Modulo);
  //Frente
  var frenteGaveta1Modulo = new THREE.Mesh(geometriaFrenteBase, materialMadeira4);
  frenteGaveta1Modulo.translateZ(9.5);
  frenteGaveta1Modulo.translateY(6);
  frenteGaveta1Modulo.translateX(40);
  scene.add(frenteGaveta1Modulo);
  //Puxador
  var puxadorModulo = new THREE.Mesh(geometriaPuxador, metal);
  puxadorModulo.translateY(5);
  puxadorModulo.translateZ(12);
  puxadorModulo.translateX(40);
  scene.add(puxadorModulo);



  /*         Gaveta 2          */
  //Base
  var baseGaveta2Modulo = new THREE.Mesh(geometriaBaseGaveta, materialMadeira4);
  baseGaveta2Modulo.translateY(1);
  baseGaveta2Modulo.translateZ(1);
  baseGaveta2Modulo.translateY(10.5);
  baseGaveta2Modulo.translateX(40);
  scene.add(baseGaveta2Modulo);
  //Lado Esquerdo
  var ladoEsquerdoGaveta2Modulo = new THREE.Mesh(geometriaLadosGaveta, materialMadeira4);
  ladoEsquerdoGaveta2Modulo.translateX(-18);
  ladoEsquerdoGaveta2Modulo.translateY(5);
  ladoEsquerdoGaveta2Modulo.translateZ(1);
  ladoEsquerdoGaveta2Modulo.translateY(10.5);
  ladoEsquerdoGaveta2Modulo.translateX(40);
  scene.add(ladoEsquerdoGaveta2Modulo);
  //Lado Direito
  var ladoDireitoGaveta2Modulo = new THREE.Mesh(geometriaLadosGaveta, materialMadeira4);
  ladoDireitoGaveta2Modulo.translateX(18);
  ladoDireitoGaveta2Modulo.translateY(5);
  ladoDireitoGaveta2Modulo.translateZ(1);
  ladoDireitoGaveta2Modulo.translateY(10.5);
  ladoDireitoGaveta2Modulo.translateX(40);
  scene.add(ladoDireitoGaveta2Modulo);
  //Tras
  var trasGaveta2Modulo = new THREE.Mesh(geometriaTrasBase, materialMadeira4);
  trasGaveta2Modulo.translateZ(-8);
  trasGaveta2Modulo.translateY(6);
  trasGaveta2Modulo.translateY(10.5);
  trasGaveta2Modulo.translateX(40);
  scene.add(trasGaveta2Modulo);
  //Frente
  var frenteGaveta2Modulo = new THREE.Mesh(geometriaFrenteBase, materialMadeira4);
  frenteGaveta2Modulo.translateZ(9.5);
  frenteGaveta2Modulo.translateY(6);
  frenteGaveta2Modulo.translateY(10.5);
  frenteGaveta2Modulo.translateX(40);
  scene.add(frenteGaveta2Modulo);
  //Puxador
  var puxador2Modulo = new THREE.Mesh(geometriaPuxador, metal);
  puxador2Modulo.translateY(5);
  puxador2Modulo.translateZ(12);
  puxador2Modulo.translateY(10.5);
  puxador2Modulo.translateX(40);
  scene.add(puxador2Modulo);

  //Barra 
  var barraModulo = new THREE.Mesh(geometryBarra, metal);
  barraModulo.translateX(40);
  barraModulo.translateY(60);
  barraModulo.rotateZ(55);
  scene.add(barraModulo);

}
